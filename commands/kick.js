module.exports = {
    name: 'kick',
    description: 'Kicks specified user.',
    execute(message, args){
       
    const user = message.mentions.users.first();

    if (user) {

        const member = message.guild.member(user);

        if (member) {

            if (message.author.roles.cache.has('ADMIN')) {
                console.log(message.member.roles);
                member
                .kick('Optional reason that will display in the audit logs')
                .then(() => {
                    message.reply(`Successfully kicked ${user.tag}`);
                })
                .catch(err => {
                    message.reply('I was unable to kick the member');
                    // Log the error
                    console.error(err);
                });

            } else {
                message.channel.send("You do not have sufficient permissions to run this command!");
            }


        } else {
            message.reply("That user isn't in this guild!");
        }
    } else {
        message.reply("You didn't mention the user to kick!");
    }
    },
};